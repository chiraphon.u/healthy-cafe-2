import React from 'react'


export default ({ input, type, lable, required, meta: { error, touched } }) => {
  return (
    <div className="form-group">
      <label className="title">{lable}</label>
      <input type={type} required={required} {...input} className="form-control" />
      {error && touched &&
        (
          <div className="mt-2 mtext-danger title">{error}</div>
        )
      }
    </div>
  )
}

